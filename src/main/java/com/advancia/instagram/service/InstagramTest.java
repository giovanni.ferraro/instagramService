package com.advancia.instagram.service;

import java.util.GregorianCalendar;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.advancia.instagram.entity.Utente;
import com.advancia.instagram.entity.Profilo;
import com.advancia.instagram.entity.Foto;
import com.advancia.instagram.util.HibernateUtil;

public class InstagramTest {
	public static void main(String[] args) {

		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = session.getTransaction();

		tx.begin();
		GregorianCalendar c = new GregorianCalendar();

		// Create Utente1 Entity
		Utente utente1 = new Utente();
		utente1.setNome("aaaa");
		utente1.setCognome("sssss");
		utente1.setSesso("Maschio");
		c.set(1991, 6, 28);
		utente1.setDataNascita(c.getTime());

		// Create Profilo1 Entity
		Profilo profilo1 = new Profilo();
		profilo1.setUserName("ababababa");
		profilo1.setPassword("1234");
		profilo1.setEmail("alfabeto@gmail.com");
		
		session.save(profilo1);		
		utente1.setProfiloUt(profilo1);		
		session.save(utente1);
//		// Create Utente2 Entity
//		Utente utente2 = new Utente();
//		utente2.setNome("Fabrizio");
//		utente2.setCognome("Trova");
//		utente2.setSesso("Maschio");
//		c.set(1995, 5, 3);
//		utente2.setDataNascita(c.getTime());
//		session.save(utente2);
//
//		// Create Profilo2 Entity
//		Profilo profilo2 = new Profilo();
//		profilo2.setUserName("fabrizioT");
//		profilo2.setPassword("ciao456");
//		profilo2.setEmail("fabrizio@gmail.com");
//
//		profilo2.setUtentePr(utente2);
//		session.save(profilo2);
//		session.save(utente2);
//		
//		// add Foto to profilo1
//		Foto foto1= new Foto();
//		foto1.setDescrizione("Foto Profilo1");
//		profilo1.addFoto(foto1);
//		
//		session.save(foto1);
//		session.save(profilo1);
//		
//		//profilo2 add like foto1 (profilo1)
//		profilo2.addLike(foto1);
//		
//		profilo1.addFollow(profilo2);

		tx.commit();
		session.close();
		HibernateUtil.getSessionFactory().close();

	}

}
