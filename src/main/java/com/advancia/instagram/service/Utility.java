package com.advancia.instagram.service;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.advancia.instagram.util.HibernateUtil;

public class Utility {
	
	private Session currentSession;
	private Transaction currentTransaction;
	
	public Session openCurrentSession() {
		currentSession = HibernateUtil.getSessionFactory().openSession();
		return currentSession;
	}
	
	public Session openCurrentSessionwithTransaction() {
		currentSession=HibernateUtil.getSessionFactory().openSession();
		currentTransaction=currentSession.beginTransaction();
		return currentSession;
	}
	
	public void closeCurrentSession() {
		currentSession.close();
	}
	

	public void closeCurrentSessionwithTransaction() {
		currentTransaction.commit();
		currentSession.close();
	}
	
	public Session getCurrentSession() {
		return currentSession;
	}
	
	public Transaction getCurrentTransaction() {
		return currentTransaction;
	}
}