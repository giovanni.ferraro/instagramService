
package com.advancia.instagram.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.jws.WebService;

import com.advancia.instagram.dao.GestioneDAO;
import com.advancia.instagram.entity.Foto;
import com.advancia.instagram.entity.FotoS;
import com.advancia.instagram.entity.Profilo;
import com.advancia.instagram.entity.ProfiloS;
import com.advancia.instagram.entity.Utente;



@WebService
public class InstagramImpl implements Instagram {

	private GestioneDAO gest = new GestioneDAO();

	@Override
	public long creaUtente(String nome, String cognome, String sesso, Date dataNascita, String userName,
			String password, String email) {
		Utente utente = gest.creaUtente(nome, cognome, sesso, dataNascita);
		Profilo profilo = gest.creaProfilo(utente, userName, password, email);
		if(profilo==null)return 0;
		return profilo.getProfiloId();
	}

	@Override
	public long login(String userName, String password) {
		return gest.login(userName, password);
	}

	@Override
	public boolean aggiungiFoto(long profiloId, String descrizione) {
		Profilo p = gest.getProfilo(profiloId);
		if(p==null)
			return false;
		Foto foto= new Foto(descrizione);
		foto.setProfiloFoto(p);
		return gest.aggiungiFoto(p, foto);
	}

	@Override
	public boolean segui(long proFollower, long proSeguito) {
		Profilo p1 = gest.getProfilo(proFollower);
		Profilo p2 = gest.getProfilo(proSeguito);
		return gest.segui(p1, p2);
	}

	@Override
	public boolean aggiungiLike(long profiloId, long  fotoId) {
		Profilo p = gest.getProfilo(profiloId);
		if(p==null) return false;
		Foto f= gest.getFoto(fotoId);
		if(f==null)return false;
		return gest.aggiungiLike(p, f);
	}

	@Override
	public ProfiloS dettagliProfilo(long profiloId) {
		Profilo profilo=gest.getProfilo(profiloId);
		if(profilo==null)return null;
		
		ProfiloS profiloS= new ProfiloS(profiloId, profilo.getEmail());
		List<Long> fotoList= new ArrayList<Long>();
		List<Long> follow= new ArrayList<Long>();
		List<Long> fotoLike= new ArrayList<Long>();
		for(Foto f:profilo.getFotoList()) {
			fotoList.add(f.getFotoId());
		}
		for(Profilo p:profilo.getFollow()) {
			follow.add(p.getProfiloId());
		}
		for(Foto f:profilo.getFotoLike()) {
			fotoLike.add(f.getFotoId());
		}
		profiloS.setFotoList(fotoList);
		profiloS.setFollow(follow);
		profiloS.setFotoLike(fotoLike);
		return profiloS;
	}
	
	@Override
	public FotoS dettagliFoto(long fotoId) {
		Foto foto=gest.getFoto(fotoId);
		if(foto==null)return null;
		FotoS fotoS=new FotoS(fotoId, foto.getDescrizione(), foto.getProfiloFoto().getProfiloId());
		List<Long> likeProfilo= new ArrayList<Long>();
		for (Profilo p: foto.getLikeProfilo()) {
			likeProfilo.add(p.getProfiloId());
		}
		
		fotoS.setLikeProfilo(likeProfilo);
		return fotoS;
	}
	
	@Override
	public List<FotoS> visualizzaElencoFoto(long profiloId) {
		Profilo profilo= gest.getProfilo(profiloId);
		if(profilo==null)return null;
		
		List<FotoS> fotoListS= new ArrayList<FotoS>();
		for(Foto f:profilo.getFotoList()) {
			FotoS fotoS= dettagliFoto(f.getFotoId());
			fotoListS.add(fotoS);
		}
		return fotoListS;
	}

	@Override
	public List<Long> visualizzaSeguiti(long profiloId) {
		Profilo profilo= gest.getProfilo(profiloId);		
		List<Long> seguiti =new ArrayList<Long>();
		if(profilo==null)return seguiti;
		for(Profilo p: profilo.getFollow()) {
			seguiti.add(p.getProfiloId());
		}
		return seguiti;
	}

	@Override
	public List<Long> visualizzaChiSegue(long profiloId) {		
		Profilo profilo = gest.getProfilo(profiloId);
		List<Long> segue=new ArrayList<Long>();
		if(profilo==null) return segue;
		for (Profilo p:gest.visualizzaFollowed(profilo)) {
			segue.add(p.getProfiloId());
		}
		return segue;
		
	}

	@Override
	public List<Long> visualizzaProfili() {
		return gest.visualizzaProfili();
	}

}
