package com.advancia.instagram.service;

import java.util.Date;
import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;

import com.advancia.instagram.entity.Profilo;
import com.advancia.instagram.entity.ProfiloS;
import com.advancia.instagram.entity.Utente;
import com.advancia.instagram.entity.Foto;
import com.advancia.instagram.entity.FotoS;

@WebService
public interface Instagram {
	
	//return profiloId
	@WebMethod
	long creaUtente(String nome, String cognome, String sesso, Date dataNascita, String userName, String password, String email);
	
	//return profiloId
	@WebMethod
	long login(String userName, String password);
	
	//return fotoId
	@WebMethod
	boolean aggiungiFoto(long profiloId, String descrizione);
	
	
	@WebMethod
	boolean segui(long proFollower, long proSeguito);
	
	@WebMethod
	boolean aggiungiLike(long profiloId, long foto);
	
	@WebMethod
	ProfiloS dettagliProfilo(long profiloId);
	
	@WebMethod
	FotoS dettagliFoto(long fotoId);
	
	@WebMethod
	List<FotoS> visualizzaElencoFoto(long profilo);
	
	@WebMethod
	List<Long> visualizzaSeguiti(long profilo);
	
	@WebMethod
	List<Long> visualizzaChiSegue(long profilo);

	@WebMethod
	List<Long> visualizzaProfili();
	
	
}


