package com.advancia.instagram.entity;

public class ProfiliR {

	private long profiloSource;
	private long profiloTarget;

	public long getProfiloTarget() {
		return profiloTarget;
	}

	public void setProfiloTarget(long profiloTarget) {
		this.profiloTarget = profiloTarget;
	}

	public long getProfiloSource() {
		return profiloSource;
	}

	public void setProfiloSource(long profiloSource) {
		this.profiloSource = profiloSource;
	}

}
