package com.advancia.instagram.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.ManyToMany;

@Entity
public class Profilo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "PROFILO_ID")
	private long profiloId;

	private String userName, password, email;

	@OneToOne(mappedBy = "profiloUt")
	private Utente utentePr;

	@OneToMany(mappedBy = "profiloFoto")
	private List<Foto> fotoList;

	@ManyToMany
	@JoinTable(name = "FOLLOW", joinColumns = @JoinColumn(name = "FOLLOWER_ID"), inverseJoinColumns = @JoinColumn(name = "FOLLOWED_ID"))
	private List<Profilo> follow;

	@ManyToMany
	@JoinTable(name = "LIKES", joinColumns = @JoinColumn(name = "PROF_LIKE_ID", referencedColumnName = "PROFILO_ID"), inverseJoinColumns = @JoinColumn(name = "FOTO_LIKE_ID", referencedColumnName = "FOTO_ID"))
	private List<Foto> fotoLike;

	public Profilo() {

	}

	public Profilo(String userName, String password, String email) {
		this.userName = userName;
		this.password = password;
		this.email = email;
	}

	public long getProfiloId() {
		return profiloId;
	}

	public void setProfiloId(long profiloId) {
		this.profiloId = profiloId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Utente getUtentePr() {
		return utentePr;
	}

	public void setUtentePr(Utente utentePr) {
		this.utentePr = utentePr;
	}

	public List<Foto> getFotoList() {
		return fotoList;
	}

	public void setFotoList(List<Foto> fotoList) {
		this.fotoList = fotoList;

		for (Foto foto : fotoList) {
			if (foto.getProfiloFoto() != this) {
				foto.setProfiloFoto(this);
			}
		}
	}

	public void addFoto(Foto foto) {
		fotoList.add(foto);
		if (foto.getProfiloFoto() != this) {
			foto.setProfiloFoto(this);
		}
	}

	public List<Profilo> getFollow() {
		return follow;
	}

	public void setFollow(List<Profilo> follow) {
		this.follow = follow;
	}

	public void addFollow(Profilo profilo) {
		follow.add(profilo);
	}

	public void removeFollow(Profilo profilo) {
		
		System.out.println(follow.contains(profilo));
		follow.remove(profilo);
		
	}

	public void removeFollow(long profiloId) {
		Profilo rem = null;
		for (Profilo p : follow) {
			if (p.getProfiloId() == profiloId) {
				rem = p;
			}
		}
		removeFollow(rem);
	}

	public List<Foto> getFotoLike() {
		return fotoLike;
	}

	public void setFotoLike(List<Foto> fotoLike) {
		this.fotoLike = fotoLike;

	}

	public void addLike(Foto foto) {
		fotoLike.add(foto);
	}

	public boolean equals(Profilo p) {
		if (this.profiloId==p.profiloId)
			return true;
		return false;
	}

}
