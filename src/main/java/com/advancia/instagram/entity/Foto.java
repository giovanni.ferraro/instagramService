package com.advancia.instagram.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

@Entity
public class Foto {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="FOTO_ID")
	private long fotoId;
	
	private String descrizione;
	
	@ManyToOne
	@JoinColumn(name="PROFILOFOTO")
	private Profilo profiloFoto;
	
	@ManyToMany(mappedBy="fotoLike")
	private List<Profilo> likeProfilo;
	
	public Foto() {
		
	}
	public Foto(String descrizione) {
		this.descrizione=descrizione;
	}
	
	public long getFotoId() {
		return fotoId;
	}
	
	public void setFotoId(long fotoId) {
		this.fotoId=fotoId;
	}
	
	public String getDescrizione() {
		return descrizione;
	}
	
	public void setDescrizione(String descrizione) {
		this.descrizione=descrizione;
	}
	
	public Profilo getProfiloFoto() {
		return profiloFoto;
	}
	
	public void setProfiloFoto(Profilo profiloFoto) {
		this.profiloFoto=profiloFoto;
		if((profiloFoto.getFotoList()==null)||(!profiloFoto.getFotoList().contains(this))) {
			profiloFoto.addFoto(this);
		}
	}
	
	public List<Profilo> getLikeProfilo(){
		return likeProfilo;
	}
	
	public void setLikeProfilo(List<Profilo> likeProfilo) {
		this.likeProfilo=likeProfilo;
	}
	
	public void addLike(Profilo profilo) {
		likeProfilo.add(profilo);
	}
	public boolean equals(Foto foto) {
		return this.fotoId==foto.getFotoId();
	}
}
