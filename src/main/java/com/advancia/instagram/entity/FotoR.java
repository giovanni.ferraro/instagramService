package com.advancia.instagram.entity;

public class FotoR {

	private long profiloId;
	private String descrizione;

	public long getProfiloId() {
		return this.profiloId;
	}

	public void setProfiloId(long profiloId) {
		this.profiloId = profiloId;
	}

	public String getDescrizione() {
		return descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

}
