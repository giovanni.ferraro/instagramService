package com.advancia.instagram.entity;

import java.util.List;

public class ProfiloS {
	private long profiloId;
	private String userName;
	private String email;
	private List<Long> fotoList;
	private List<Long> follow;
	private List<Long> fotoLike;
	
	public ProfiloS() {
		
	}
	
	public ProfiloS(long profiloId, String email) {
		this.profiloId=profiloId;
		this.email=email;
	}
	
	public long getProfiloId() {
		return profiloId;
	}
	
	public void setProfiloId(long profiloId) {
		this.profiloId=profiloId;
	}
		
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email=email;
	}
	
	
	public List<Long> getFotoList(){
		return fotoList;
	}
	
	public void setFotoList(List<Long> fotoList) {
		this.fotoList=fotoList;
	}
	
	public void addFoto(long foto) {
		fotoList.add(foto);
	}
	
	public List<Long> getFollow(){
		return follow;
	}
	
	public void setFollow(List<Long> follow) {
		this.follow=follow;
	}
	
	public void addFollow(long profilo) {
		follow.add(profilo);
	}
	
	public List<Long> getFotoLike(){
		return fotoLike;
	}
	
	public void setFotoLike(List<Long> fotoLike) {
		this.fotoLike=fotoLike;
	}
	
	public void addLike(long foto) {
		fotoLike.add(foto);
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

}
