package com.advancia.instagram.entity;

import java.util.List;

public class FotoS {

	private long fotoId;
	private String descrizione;
	private long profiloFoto;
	private List<Long> likeProfilo;
	
	public FotoS() {
		
	}
	public FotoS(long fotoId, String descrizione, long profiloFoto) {
		this.fotoId=fotoId;
		this.descrizione=descrizione;
		this.profiloFoto=profiloFoto;
	}
	
	public long getFotoId() {
		return fotoId;
	}
	
	public void setFotoId(long fotoId) {
		this.fotoId=fotoId;
	}
	
	public String getDescrizione() {
		return descrizione;
	}
	
	public void setDescrizione(String descrizione) {
		this.descrizione=descrizione;
	}
	
	public long getProfiloFoto() {
		return profiloFoto;
	}
	
	public void setProfiloFoto(long profiloFoto) {
		this.profiloFoto=profiloFoto;
	}
	
	public List<Long> getLikeProfilo(){
		return likeProfilo;
	}
	
	public void setLikeProfilo(List<Long> likeProfilo) {
		this.likeProfilo=likeProfilo;
	}
	
	public void addLike(long profilo) {
		likeProfilo.add(profilo);
	}

	
	
}
