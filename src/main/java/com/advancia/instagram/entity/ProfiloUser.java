package com.advancia.instagram.entity;

public class ProfiloUser {

	private long profiloId;
	private String userName;
	
	public ProfiloUser(long profiloId, String userName) {
		this.setProfiloId(profiloId);
		this.setUserName(userName);
	}

	public long getProfiloId() {
		return profiloId;
	}

	public void setProfiloId(long profiloId) {
		this.profiloId = profiloId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
}
