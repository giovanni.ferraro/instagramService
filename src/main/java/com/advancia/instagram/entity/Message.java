package com.advancia.instagram.entity;

public class Message {
	
	private long code;	
	private String message;
	
	public Message() {
		
	}
	
	public Message(long code, String message) {
		setCode(code);
		setMessage(message);
	}
	
	public void setCode(long code) {
		this.code=code;
	}
	
	public long getCode() {
		return code;
	}
	
	public void setMessage(String message) {
		this.message=message;
	}
	
	public String getMessage() {
		return message;
	}

}
