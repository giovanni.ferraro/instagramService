package com.advancia.instagram.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class Utente {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="UTENTE_ID")
	private long utenteId;
	private String nome, cognome, sesso;
	private Date dataNascita;
	
	@OneToOne
	@JoinColumn(name= "PROF_UT_ID")
	private Profilo profiloUt;
	
	public Utente() {
		
	}

	public Utente (String nome, String cognome,Date dataNascita, String sesso) {
		
		this.nome=nome;
		this.cognome=cognome;
		this.dataNascita=dataNascita;
		this.sesso=sesso;
	}
	
	public long getUtenteId() {
		return utenteId;
	}
	
	public void setUtenteId(long utenteId) {
		this.utenteId=utenteId;
	}

	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome=nome;
	}
	
	public String getCognome() {
		return cognome;
	}
	
	public void setCognome(String cognome) {
		this.cognome=cognome;
	}

	public Date getDataNascita() {
		return dataNascita;
	}
	
	public void setDataNascita(Date dataNascita) {
		this.dataNascita=dataNascita;
	}
	
	public String getSesso() {
		return sesso;
	}
	
	public void setSesso(String sesso) {
		this.sesso=sesso;
	}
	
	public Profilo getProfiloUt() {
		return profiloUt;
	}
	
	public void setProfiloUt(Profilo profiloUt) {
		this.profiloUt=profiloUt;
//		if(profiloUt.getUtentePr()!= this) {//
//			profiloUt.setUtentePr(this);
//		}
	}
}

