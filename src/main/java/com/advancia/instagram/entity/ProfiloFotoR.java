package com.advancia.instagram.entity;

public class ProfiloFotoR {

	private long profiloId;
	private long fotoId;

	public long getProfiloId() {
		return profiloId;
	}

	public void setProfiloId(long profiloId) {
		this.profiloId = profiloId;
	}

	public long getFotoId() {
		return fotoId;
	}

	public void setFotoId(long fotoId) {
		this.fotoId = fotoId;
	}

}
