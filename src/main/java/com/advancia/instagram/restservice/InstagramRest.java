package com.advancia.instagram.restservice;

import java.security.Key;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.crypto.spec.SecretKeySpec;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.advancia.instagram.dao.GestioneDAO;
import com.advancia.instagram.entity.Foto;
import com.advancia.instagram.entity.FotoR;
import com.advancia.instagram.entity.FotoS;
import com.advancia.instagram.entity.Login;
import com.advancia.instagram.entity.Message;
import com.advancia.instagram.entity.ProfiliR;
import com.advancia.instagram.entity.Profilo;
import com.advancia.instagram.entity.ProfiloFotoR;
import com.advancia.instagram.entity.ProfiloS;
import com.advancia.instagram.entity.ProfiloUser;
import com.advancia.instagram.entity.Utente;
import com.advancia.instagram.entity.UtenteR;
import com.advancia.instagram.security.JsonWebToken;

import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.TextCodec;
import io.jsonwebtoken.impl.crypto.MacProvider;

@Path("/json")
public class InstagramRest {
	
	private GestioneDAO gest = new GestioneDAO();
	private byte[] key = TextCodec.BASE64.decode("Yn2kjibddFAWtnPJ2AFlL8WXmohJMCvigQggaEypa5E=");

	@POST
	@Path("/creaUtente")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response creaUtente(UtenteR ut) {
		Message msg = new Message();
		DateFormat df = new SimpleDateFormat("yyyy/MM/dd");
		DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd");
		Date date;
		try {
			date = df.parse(ut.getDataNascita());
		} catch (ParseException e) {
			try {
				date = df1.parse(ut.getDataNascita());
			} catch (ParseException e1) {
				e1.printStackTrace();
				msg.setMessage(
						"Creazione utente fallita! Formato data errato, formati consentiti: \"yyyy/MM/dd\" o \"yyyy-MM-dd\"");
				return Response.status(Status.BAD_REQUEST).entity(msg).build();
			}
		}
		Utente utente = gest.creaUtente(ut.getNome(), ut.getCognome(), ut.getSesso(), date);
		Profilo profilo = gest.creaProfilo(utente, ut.getUserName(), ut.getPassword(), ut.getEmail());
		if (profilo == null) {
			msg.setMessage("Creazione Utente fallita!");
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(msg).build();
		}
		msg.setMessage("Creazione avvenuta con successo! profilo creato");
		msg.setCode(profilo.getProfiloId());
		return Response.status(Status.OK).entity(msg).build();
	}

	@POST
	@Path("/login")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response loginP(Login accesso) {
		Message msg = new Message();
		long result = gest.login(accesso.getUserName(), accesso.getPassword());
		if (result == 0) {
			msg.setMessage("Dati errati");
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(msg).build();
		}
		
		String token = new JsonWebToken(key, result).createJWT();
		
		msg.setCode(result);
		msg.setMessage("Login effettuato da: ");
		return Response.status(Status.OK).header("AUTHORIZATION", "Bearer " + token).entity(msg).build();
	}

	@POST
	@Path("/aggiungiFoto")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response aggiungiFoto(FotoR newFoto) {
		Message msg = new Message();
		Profilo p = gest.getProfilo(newFoto.getProfiloId());
		if (p == null) {
			msg.setMessage("Aggiunta foto fallita! Il codice profilo non corrisponde con nessun profilo esistente.");
		} else {
			Foto foto = new Foto(newFoto.getDescrizione());
			if (gest.aggiungiFoto(p, foto)) {
				msg.setMessage("Foto aggiunta con successo.");
				msg.setCode(foto.getFotoId());
				return Response.status(Status.OK).entity(msg).build();
			}
		}
		msg.setMessage("Aggiunta foto fallita!");
		return Response.status(Status.INTERNAL_SERVER_ERROR).entity(msg).build();
	}

	@POST
	@Path("/segui")

	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response segui(ProfiliR profili) {
		Message msg = new Message();
		Profilo p1 = gest.getProfilo(profili.getProfiloSource());
		Profilo p2 = gest.getProfilo(profili.getProfiloTarget());
		if (p1 == null || p2 == null) {
			msg.setMessage("Operazione fallita! dati errati.");
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(msg).build();
		}
		if (gest.segui(p1, p2)) {
			msg.setCode(1);
			msg.setMessage("Operazione Eseguita con successo.");
			return Response.status(Status.OK).entity(msg).build();
		}
		msg.setMessage("Operazione fallita!");
		return Response.status(Status.INTERNAL_SERVER_ERROR).entity(msg).build();
	}

	@POST
	@Path("/rimuoviSegui")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response rimuoviSegui(ProfiliR profili) {
		Message msg = new Message();
		int result = gest.rimuoviSegui(profili.getProfiloSource(), profili.getProfiloTarget());
		if (result == -1) {
			msg.setMessage("Operazione non riuscita!");
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(msg).build();
		}
		if (result == 0) {
			msg.setMessage("Il segui che stai tentando di rimuovere non esiste");
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(msg).build();
		}
		msg.setMessage("Rimozione segui riuscita");
		msg.setCode(1);
		return Response.status(Status.OK).entity(msg).build();
	}

	@POST
	@Path("/aggiungiLike")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response aggiungiLike(ProfiloFotoR profFoto) {
		Message msg = new Message();
		Profilo p = gest.getProfilo(profFoto.getProfiloId());
		if (p == null) {
			msg.setMessage("Operazione Fallita! Profilo inesistente.");
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(msg).build();
		}
		Foto f = gest.getFoto(profFoto.getFotoId());
		if (f == null) {
			msg.setMessage("Operazione fallita! Foto inesistente.");
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(msg).build();
		}
		if (gest.aggiungiLike(p, f)) {
			msg.setCode(1);
			msg.setMessage("Operazione eseguita con successo.");
			return Response.status(Status.OK).entity(msg).build();

		}
		msg.setMessage("Operazione fallita!");
		return Response.status(Status.INTERNAL_SERVER_ERROR).entity(msg).build();
	}

	@POST
	@Path("/rimuoviLike")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response rimuoviLike(ProfiloFotoR profFoto) {
		Message msg = new Message();
		int result = gest.rimuoviLike(profFoto.getProfiloId(), profFoto.getFotoId());
		if (result == -1) {
			msg.setMessage("Operazione fallita!");
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(msg).build();
		}
		if (result == 0) {
			msg.setMessage("Il like che vuoi rimuovere non esiste");
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(msg).build();
		}
		msg.setCode(1);
		msg.setMessage("Rimozione like riuscita");
		return Response.status(Status.OK).entity(msg).build();

	}

	@GET
	@Path("/dettagliProfilo/{profiloId}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response dettagliProfilo(@PathParam("profiloId") long profiloId) {
		Message msg = new Message();
		Profilo profilo = gest.getProfilo(profiloId);
		if (profilo == null) {
			msg.setMessage("Operazione fallita! Profilo inesistente.");
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(msg).build();
		}
		ProfiloS profiloS = new ProfiloS(profiloId, profilo.getEmail());
		profiloS.setUserName(profilo.getUserName());
		List<Long> fotoList = new ArrayList<Long>();
		List<Long> follow = new ArrayList<Long>();
		List<Long> fotoLike = new ArrayList<Long>();
		for (Foto f : profilo.getFotoList()) {
			fotoList.add(f.getFotoId());
		}
		for (Profilo p : profilo.getFollow()) {
			follow.add(p.getProfiloId());
		}
		for (Foto f : profilo.getFotoLike()) {
			fotoLike.add(f.getFotoId());
		}
		profiloS.setFotoList(fotoList);
		profiloS.setFollow(follow);
		profiloS.setFotoLike(fotoLike);
		return Response.status(Status.OK).entity(profiloS).build();
	}

	@GET
	@Path("/dettagliFoto/{fotoId}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response dettagliFoto(@PathParam("fotoId") long fotoId) {
		Message msg = new Message();
		Foto foto = gest.getFoto(fotoId);
		if (foto == null) {
			msg.setMessage("Operaione fallita! Foto inesistente.");
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(msg).build();
		}
		FotoS fotoS = new FotoS(fotoId, foto.getDescrizione(), foto.getProfiloFoto().getProfiloId());
		List<Long> likeProfilo = new ArrayList<Long>();
		for (Profilo p : foto.getLikeProfilo()) {
			likeProfilo.add(p.getProfiloId());
		}

		fotoS.setLikeProfilo(likeProfilo);
		return Response.status(Status.OK).entity(fotoS).build();
	}

	@GET
	@Path("/visualizzaElencoFoto/{profiloId}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response visualizzaElencoFoto(@PathParam("profiloId") long profiloId) {
		Message msg = new Message();

		List<FotoS> fotoList = gest.visualizzaFoto(profiloId);
		if (fotoList == null) {
			msg.setMessage("Operazione fallita!");
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(msg).build();
		}
		return Response.status(Status.OK).entity(fotoList).build();
	}

	@GET
	@Path("/visualizzaSeguiti/{profiloId}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response visualizzaSeguiti(@PathParam("profiloId") long profiloId) {
		Message msg = new Message();
		Profilo profilo = gest.getProfilo(profiloId);
		if (profilo == null) {
			msg.setMessage("Profilo inesistente");
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(msg).build();
		}
		List<ProfiloUser> seguiti = new ArrayList<ProfiloUser>();
		for (Profilo p : profilo.getFollow()) {
			seguiti.add(new ProfiloUser(p.getProfiloId(), p.getUserName()));
		}
		return Response.status(Status.OK).entity(seguiti).build();
	}

	@GET
	@Path("/visualizzaChiSegue/{profiloId}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response visualizzaChiSegue(@PathParam("profiloId") long profiloId) {
		Profilo profilo = gest.getProfilo(profiloId);
		if (profilo == null) {
			Message msg = new Message();
			msg.setMessage("Operazione fallita, profilo inesistente.");
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(msg).build();
		}
		List<Long> segue = new ArrayList<Long>();
		for (Profilo p : gest.visualizzaFollowed(profilo)) {
			segue.add(p.getProfiloId());
		}
		return Response.status(Status.OK).entity(segue).build();

	}

	@GET
	@Path("/visualizzaProfili")
	@Produces(MediaType.APPLICATION_JSON)
	public Response visualizzaProfili(@Context HttpHeaders headers) {
		// public Response visualizzaProfili(@HeaderParam("AUTHORIZATION") String
		// authorization) {
		Message msg = new Message();

		// try {
		// Jwts.parser().setSigningKey(key).parseClaimsJws(authorization);
		// }catch(SignatureException e) {
		// msg.setMessage("Accesso non autorizzato");
		// return Response.status(Status.INTERNAL_SERVER_ERROR).entity(msg).build();
		// }

		List<Long> result = gest.visualizzaProfili();
		if (result.isEmpty()) {
			msg.setMessage("Non ci sono Profili registrati");
			return Response.status(Status.OK).entity(msg).build();
		}
		return Response.status(Status.OK).entity(result).build();
	}

	@DELETE
	@Path("/eliminaFoto/{fotoId}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response eliminaFoto(@PathParam("fotoId") long fotoId) {
		Message msg = new Message();
		if (gest.eliminaFoto(fotoId)) {
			msg.setMessage("Operazione eseguita con successo. Foto eliminata: ");
			msg.setCode(fotoId);
			return Response.status(Status.OK).entity(msg).build();
		}
		msg.setMessage("Operazione fallita!");
		return Response.status(Status.INTERNAL_SERVER_ERROR).entity(msg).build();
	}

	@PUT
	@Path("/modificaFoto")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response modificaFoto(FotoS foto) {
		Message msg = new Message();
		if (gest.modificaFoto(foto.getFotoId(), foto.getDescrizione())) {
			msg.setMessage("Operazione eseguita con successo. Foto modificata: ");
			msg.setCode(foto.getFotoId());
			return Response.status(Status.OK).entity(msg).build();
		}
		msg.setMessage("Operazione Fallita. Id foto non trovato: ");
		msg.setCode(foto.getFotoId());
		return Response.status(Status.INTERNAL_SERVER_ERROR).entity(msg).build();
	}

	@DELETE
	@Path("/eliminaProfilo/{profiloId}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response eliminaProfilo(@PathParam("profiloId") long profiloId) {
		Message msg = new Message();

		if (gest.eliminaProfilo(profiloId)) {
			msg.setCode(profiloId);
			msg.setMessage("Profilo eliminato correttamente.");
			return Response.status(Status.OK).entity(msg).build();
		}
		msg.setMessage("Operazione fallita!");
		return Response.status(Status.INTERNAL_SERVER_ERROR).entity(msg).build();
	}

}
