package com.advancia.instagram.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import com.advancia.instagram.entity.*;
import com.advancia.instagram.service.*;

public class GestioneDAO {

	Utility ut;

	public Utente creaUtente(String nome, String cognome, String sesso, Date dataNascita) {
		Utente utente = new Utente(nome, cognome, dataNascita, sesso);
		ut = new Utility();
		ut.openCurrentSessionwithTransaction();
		ut.getCurrentSession().save(utente);
		ut.closeCurrentSessionwithTransaction();
		return utente;
	}

	public Profilo creaProfilo(Utente utente, String userName, String password, String email) {
		if (utente==null)return null;
		Profilo profilo = new Profilo(userName, password, email);
		profilo.setUtentePr(utente);
		ut = new Utility();
		try {
			ut.openCurrentSessionwithTransaction();
			ut.getCurrentSession().save(profilo);
			utente.setProfiloUt(profilo);
			ut.getCurrentSession().update(utente);
			ut.closeCurrentSessionwithTransaction();
			return profilo;
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public Utente getUtente(long id) {
		ut = new Utility();
		ut.openCurrentSession();
		Query query = ut.getCurrentSession().createQuery("select from Utente u where u.utenteId= :id");
		query.setParameter("userName", id);
		List<Utente> result=(List<Utente>) query.list();
		if(result.size()==0) return null;
		Utente utente =result.get(0);
		ut.closeCurrentSession();
		return utente;
	}
	
	@SuppressWarnings("unchecked")
	public long login(String userName, String password) {
		ut = new Utility();
		ut.openCurrentSession();
		String q = "Select p.profiloId from Profilo p where p.userName = :userName and p.password = :password";
		Query query = ut.getCurrentSession().createQuery(q);
		query.setParameter("userName", userName).setParameter("password", password);

		List<Long> result = (List<Long>) query.list();
		if(result.size()==0)
			return 0;
		long profiloid = result.get(0);

		ut.closeCurrentSession();
		return profiloid;
	}

	public Profilo getProfilo(long id) {
		ut = new Utility();
		ut.openCurrentSession();
		Query query=ut.getCurrentSession().createQuery("select p from Profilo p left join fetch p.follow where p.profiloId = :id");
		query.setParameter("id", id);
		List<Profilo> result=(List<Profilo>) query.list();
		if(result.size()==0) return null;
		Profilo profilo =result.get(0);
		for(Foto f:profilo.getFotoLike())
			f.getProfiloFoto();
		for (Foto f: profilo.getFotoList()) {
			f.getFotoId();
		}
		ut.closeCurrentSession();
		return profilo;
	}

	public Foto getFoto(long id) {
		ut = new Utility();
		ut.openCurrentSession();
		
		Query query=ut.getCurrentSession().createQuery("select f from Foto f left join fetch f.likeProfilo where f.fotoId = :id");
		query.setParameter("id", id);
		List<Foto> result=(List<Foto>) query.list();
		if(result.size()==0) return null;
		Foto foto= result.get(0);
		for(Profilo p:foto.getLikeProfilo()) {
			p.getFotoLike();
		}
		ut.closeCurrentSession();
		return foto;
	}

	public boolean aggiungiFoto(Profilo profilo, Foto foto) {
		if(profilo==null)return false;
		if (profilo.getFotoList() == null) {
			profilo.setFotoList(new ArrayList<Foto>());
		}
		try {
			ut = new Utility();
			ut.openCurrentSessionwithTransaction();
			profilo.addFoto(foto);
			ut.getCurrentSession().update(profilo);
			foto.setProfiloFoto(profilo);
			ut.getCurrentSession().save(foto);
			ut.closeCurrentSessionwithTransaction();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	// ritorna true se p1 segue p2, false altrimenti
	public boolean isFollower(Profilo p1, Profilo p2) {
		if(p1==null||p2==null)return false;
		
		return visualizzaFollower(p1).contains(p2);
	}

	public boolean segui(Profilo follower, Profilo followed) {
		if(follower==null||followed==null)
			return false;
		if (follower.getFollow() == null) {
			follower.setFollow(new ArrayList<Profilo>());
		}
		try {
			ut = new Utility();
			ut.openCurrentSessionwithTransaction();
			follower.addFollow(followed);
			ut.getCurrentSession().update(follower);
			ut.closeCurrentSessionwithTransaction();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public int rimuoviSegui(long followerId, long followedId) {
		Profilo follower= getProfilo(followerId);
		Profilo followed= getProfilo(followedId);
		int res=0;
		if(follower==null)
			return -1;
		if(follower.getFollow()==null||follower.getFollow().isEmpty()) {
			return res;
		}
		try {
			ut = new Utility();
			ut.openCurrentSessionwithTransaction();

			if(followed!=null) {
				Profilo rem=null;
				for(Profilo p:follower.getFollow()) {
					if (p.equals(followed));
					rem=p;
				}
				follower.getFollow().remove(rem);
				res=1;
			}else {
				Profilo rem=null;
				for(Profilo p: follower.getFollow()) {
					if(p.getProfiloId()==followedId) {
						rem=p;
						
					}
					follower.getFollow().remove(rem);
					res=1;
				}
			}
			
			ut.getCurrentSession().update(follower);
			System.out.println(followed.getProfiloId());
			ut.closeCurrentSessionwithTransaction();
			return res;
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
		
	}

	public boolean aggiungiLike(Profilo profilo, Foto foto) {
		if(profilo==null||foto==null)return false;
		if (profilo.getFotoLike() == null) {
			profilo.setFotoLike(new ArrayList<Foto>());
		}
		if (foto.getLikeProfilo() == null) {
			foto.setLikeProfilo(new ArrayList<Profilo>());
		}
		try {
			ut = new Utility();
			ut.openCurrentSessionwithTransaction();
			profilo.addLike(foto);
			ut.getCurrentSession().update(profilo);
			foto.addLike(profilo);
			ut.getCurrentSession().update(foto);
			ut.closeCurrentSessionwithTransaction();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public int rimuoviLike(long profiloId, long fotoId) {
		Profilo profilo=getProfilo(profiloId);
		Foto foto=getFoto(fotoId);
		int res=0;
		if (profilo==null)return -1;
		if (profilo.getFotoLike() == null) {
			System.out.println("profilo.getfotolike null");
			return res;
		}		
		try {
			ut = new Utility();
			ut.openCurrentSessionwithTransaction();
			Foto rem = null;
			if(foto!=null) {
				for (Foto f : profilo.getFotoLike()) {
					if (f.equals(foto)) {
						rem = f;
						res=1;
					}
				}		
				foto.getLikeProfilo().remove(profilo);
				
			}else {
				for (Foto f : profilo.getFotoLike()) {
					if (f.getFotoId() == fotoId) {
						rem = f;
						res=1;
					}
				}
			}
			profilo.getFotoLike().remove(rem);
			ut.getCurrentSession().update(profilo);
			if(foto!=null) {
				ut.getCurrentSession().update(foto);
			}
			ut.closeCurrentSessionwithTransaction();
			return res;
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
	}

	public List<FotoS> visualizzaFoto(long profiloId) {
		List<FotoS> fotoListS= new ArrayList<FotoS>();
		
		ut = new Utility();
		ut.openCurrentSession();
		
		Query query=ut.getCurrentSession().createQuery("select p.fotoList from Profilo p where p.profiloId = :id");
		query.setParameter("id", profiloId);
		List<Foto> result=(List<Foto>) query.list();
		if(result.size()==0) return fotoListS;
		
		for(Foto f:result) {
			FotoS fotoS= new FotoS();
			fotoS.setFotoId(f.getFotoId());
			fotoS.setDescrizione(f.getDescrizione());
			fotoS.setProfiloFoto(profiloId);
			List<Long> likeProfilo= new ArrayList<Long>();
			for (Profilo p:f.getLikeProfilo()) {
				likeProfilo.add(p.getProfiloId());
			}
			fotoS.setLikeProfilo(likeProfilo);
			fotoListS.add(fotoS);
		}
		return fotoListS;
	}

	public List<Profilo> visualizzaLike(Foto foto) {
		if(foto==null)return null;
		if (foto.getLikeProfilo() == null) {
			return new ArrayList<Profilo>();
		}
		return foto.getLikeProfilo();
	}

	// visualizza i profili seguiti da profilo
	public List<Profilo> visualizzaFollower(Profilo profilo) {
		List<Profilo> follower = new ArrayList<Profilo>();
		if(profilo==null)return follower;
		if (profilo.getFollow() == null) {
			return follower;
		}
		return profilo.getFollow();
	}

	// visualizza i profili che seguono profilo
	@SuppressWarnings("unchecked")
	public List<Profilo> visualizzaFollowed(Profilo profilo) {
		ArrayList<Profilo> followed = new ArrayList<Profilo>();
		if(profilo==null)return followed;
		ut = new Utility();
		ut.openCurrentSession();
		List<Profilo> profili = (List<Profilo>) ut.getCurrentSession().createQuery("select p from Profilo p").list();
		
		for (Profilo p : profili) {
			for(Profilo prof2: p.getFollow()) {
				if(prof2.getProfiloId()==profilo.getProfiloId())
					followed.add(p);
			}
		}
		ut.closeCurrentSession();
		return followed;
	}
	
	public List<Long> visualizzaProfili(){
		List<Long> profiliId= new ArrayList<Long>();
		ut = new Utility();
		ut.openCurrentSession();
		List<Profilo> profili = (List<Profilo>) ut.getCurrentSession().createQuery("select p from Profilo p").list();
		for (Profilo p : profili) {
			profiliId.add(p.getProfiloId());
		}
		ut.closeCurrentSession();
		return profiliId;
	}
	
	public boolean eliminaFoto(long fotoId) {
		
		ut = new Utility();
		ut.openCurrentSessionwithTransaction();
		Query query = ut.getCurrentSession().createQuery("select f from Foto f where f.fotoId= :id");
		query.setParameter("id", fotoId);
		List<Foto> result= query.list();
		if(result.size()==0) {
			return false;
		}
		for (Foto f: result) {
			
			Query query2 = ut.getCurrentSession().createQuery("select p from Profilo p");
			List<Profilo> result2= query2.list();
			for( Profilo p: result2) {
				p.getFotoLike().remove(f);
				ut.getCurrentSession().update(p);
			}
			
			ut.getCurrentSession().delete(f);
		}
		ut.closeCurrentSessionwithTransaction();
		return true;
	}
	
	public boolean modificaFoto(long fotoId, String descrizione) {
		ut = new Utility();
		ut.openCurrentSessionwithTransaction();
		Query query = ut.getCurrentSession().createQuery("select f from Foto f where f.fotoId= :id");
		query.setParameter("id", fotoId);
		List<Foto> result= query.list();
		if(result.size()==0) {
			return false;
		}
		Foto foto = result.get(0);
		foto.setDescrizione(descrizione);
		ut.getCurrentSession().update(foto);
		ut.closeCurrentSessionwithTransaction();
		return true;
	}
	
	public boolean eliminaProfilo(long profiloId) {
		
		try {
			ut= new Utility();
			ut.openCurrentSessionwithTransaction();	
			
			Query query=ut.getCurrentSession().createQuery("select p from Profilo p where p.profiloId = :id");
			query.setParameter("id", profiloId);
			List<Profilo> result=(List<Profilo>) query.list();
			if(result.size()==0) return false;
			Profilo profilo =result.get(0);
			
			ut.getCurrentSession().delete(profilo.getUtentePr());//elimina utente
			
			if(profilo.getFotoList()!=null) {
				for(Foto f:profilo.getFotoList()) {
					if(f.getLikeProfilo()!=null) {
						for(Profilo p: f.getLikeProfilo()) {
							p.getFotoLike().remove(f);
						}// elimina like della foto
						f.getLikeProfilo().clear();
					}
					ut.getCurrentSession().delete(f);
				}//elimina singola foto
			}//elimina lista foto
			
			if(profilo.getFollow()!=null) {
				
				for(Profilo p: profilo.getFollow()) {
					p.removeFollow(profilo);
				}
				profilo.getFollow().clear();
			}
			if(profilo.getFotoLike()!=null) {
				for(Foto f: profilo.getFotoLike()) {
					f.getLikeProfilo().remove(profilo);
				}
			}
			
			ut.getCurrentSession().delete(profilo);
			ut.closeCurrentSessionwithTransaction();
			return true;
		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}



}
