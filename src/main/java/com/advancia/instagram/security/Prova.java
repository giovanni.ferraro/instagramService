package com.advancia.instagram.security;

import java.security.Key;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import io.jsonwebtoken.SignatureAlgorithm;

public class Prova {	
	
	public static void main (String []args) {
		 //We will sign our JWT with our ApiKey secret
	    byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary("chiave");
	    Key signingKey = new SecretKeySpec(apiKeySecretBytes, SignatureAlgorithm.HS256.getJcaName());
		
	    Security s=new Security();
		String stringa= s.createJWT("12345", "issuer", "subject",1000000, signingKey);
		System.out.println(stringa);
		
		
	}
}
