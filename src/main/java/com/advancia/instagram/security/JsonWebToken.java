package com.advancia.instagram.security;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.crypto.MacProvider;
import java.security.Key;

public class JsonWebToken {
	
	private long profiloId;
	private byte[] key;
	
	public JsonWebToken(byte[] key2,long profiloId) {
		this.profiloId=profiloId;
		this.key=key2;
		
	}
	
	public String createJWT() {
			String compactJws= Jwts.builder()
			  .setSubject(""+profiloId)
			  .signWith(SignatureAlgorithm.HS512, key)
			  .compact();
			return compactJws;
	}

}
